<?php
include('helpers/functions.php');
?>

<html>
    <head>
        <?php include 'templates/header.php';?>
    </head>

    <body>
    <?php include 'templates/cover.php';?>
    <?php include 'templates/chapter-intro.php';?>
    <?php include 'templates/chapter.php';?>
    <?php include 'templates/footer.php';?>

    <script src="static/dist/js.min.js"></script>
    </body>
</html>

