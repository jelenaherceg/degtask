<?php
/**
 * @param $template "views/neki_template.php"
 * @param $param ['param1' => 'neka_vrijednost_tog_parametra]
 */
function render($template, $EXTRACTION_PARAMS)
{
    $defaultUrl = 'templates/';
    $template = $defaultUrl . $template . '.php';
    ob_start();
    //extract everything in param into the current scope
    extract($EXTRACTION_PARAMS, EXTR_SKIP);
    include $template;
    return ob_get_clean();
}