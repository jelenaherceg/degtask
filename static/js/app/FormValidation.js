class FormValidation extends BaseClass{
    constructor() {
        super();
        this.init();
    }

    init() {
        $('.form').submit(function(event){
            let name = $(this).find('#name');
            let email = $(this).find('#email');
            let message = $(this).find('#message');

            let submit = true;

            if (name.val().length == 0) {
                $(this).find('.input-name').find('.validation-message').addClass('is-active');
                submit = false;
            }else {
                $(this).find('.input-name').find('.validation-message').removeClass('is-active');
            }
            if (email.val().length == 0) {
                $(this).find('.input-email').find('.validation-message').addClass('is-active');
                submit = false;
            }else {
                $(this).find('.input-email').find('.validation-message').removeClass('is-active');
            }
            if (message.val().length == 0) {
                $(this).find('.input-message').find('.validation-message').addClass('is-active');
                submit = false;
            }else {
                $(this).find('.input-message').find('.validation-message').removeClass('is-active');
            }

           return submit;
        });
    }
}