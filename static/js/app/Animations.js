class Animations extends BaseClass{
    constructor(){
        super();
        this.cover();
    }

    onScroll() {
        super.onScroll();
        // console.log('okida')
    }

    onScrollUp() {
        this.resetChapterAnimations();
    }

    onResize() {
        super.onResize();
    }

    cover() {
        this.menuOpened = false;
        this.socialsOpened = false;
        this.aboutAuthorOpened = false;

        let self = this;

        $('.header-menu-link').on('click', function () {
            self.menuClick(this);
        });

        $('.header-chapters-link').on('click', function () {
            self.chaptersClick(this);
        });

        $('.chapter-menu-container-close').on('click', function () {
            self.chaptersClick(this, true);
        });

        $('.explore-more').on('click', function () {
            self.scrollToNext();
        });

        $('.share').on('click', function () {
            self.socials();
        });

        $('.about-author').on('click', function () {
            self.aboutAuthor();
        });

        $('.about-author-popup-container .close').on('click', function () {
            self.aboutAuthor(true);
        })
    }

    menuClick(elem, close = null) {
        let animationTime = 0.5;

        if (close != null) {
            this.menuOpened = close
        }

        if (!this.menuOpened) {
            this.menuOpened = true;
            $(elem).find('button').addClass('is-active');
            $('.main-menu').addClass('is-active');
            TweenMax.set('.menu-container .staggerAnimation', {marginLeft: '-100%'});
            TweenMax.to('.transparent-background', animationTime, {opacity:1});
            TweenMax.to('.menu-container', animationTime, {left:0, onComplete: function () {
            }});
            TweenMax.staggerTo('.menu-container .staggerAnimation', animationTime/1.2, {marginLeft: 0, ease: 'easeOut'}, 0.14);

        }else {
            $(elem).find('button').removeClass('is-active');
            TweenMax.to('.menu-container', animationTime, {left:'-100%', onComplete: function () {
                $('.main-menu').removeClass('is-active');
            }});
            TweenMax.to('.transparent-background', animationTime, {opacity:0});
            this.menuOpened = false;
        }
    }

    chaptersClick(elem, close = null) {
        let animationTime = 0.5;

        if (close != null) {
            this.menuOpened = close
        }

        if (!this.menuOpened) {
            this.menuOpened = true;
            $('.chapter-menu-container').addClass('is-active');
            TweenMax.set('.chapter-menu-container .chapter-menu', {scale: 0, 'margin-left': '60%', 'margin-top': '-30%'});
            TweenMax.set('.chapter-menu-container .staggerAnimation', {'margin-top': '100px', opacity: 0});
            TweenMax.set('.chapter-menu-container .chapter-menu-container-close', {opacity: 0});
            TweenMax.to('.chapter-menu-container .chapter-menu', animationTime, {
                scale: 1,
                'margin-left': 0,
                'margin-top': 0,
                onComplete: function () {
                TweenMax.to('.chapter-menu-container .chapter-menu-container-close', animationTime, {opacity: 1});
                TweenMax.staggerTo('.chapter-menu-container .staggerAnimation', animationTime, {'margin-top': 0, opacity: 1}, 0.1);

            }});

        }else {
            this.menuOpened = false;
            TweenMax.set('.chapter-menu-container .chapter-menu-container-close', {opacity: 0});
            TweenMax.to('.chapter-menu-container .chapter-menu', animationTime, {
                scale: 0,
                'margin-left': '55%',
                'margin-top': '-35%',
                onComplete: function () {
                    $('.chapter-menu-container').removeClass('is-active');
                }
            });
        }
    }

    scrollToNext() {
        TweenLite.set('#cover', {overflow: 'hidden'});
        TweenLite.to(window, 1.5, {scrollTo:'#chapter-intro'});
        TweenLite.to('#cover .cover-container', 1, {
            'margin-top': '5%',
        });
        TweenLite.to('#cover', 1.6, {
            opacity: 0.5,
            onComplete: function () {
                TweenLite.set('#cover .cover-container', {'margin-top': 0,});
                TweenLite.set('#cover', {'opacity': 1,});
            }
        });
    }

    resetChapterAnimations() {
        TweenLite.set('#cover .cover-container', {'margin-top': 0,});
        TweenLite.set('#cover', {'opacity': 1,});
    }

    socials(close = null) {
        let animationSpeed = 0.3;
        if (close != null) {
            this.socialsOpened = close
        }

        if (!this.socialsOpened) {
            this.socialsOpened = true;
            $('.hidden-socials').addClass('is-active');
            TweenLite.set('.hidden-socials', { 'margin-right': '-60px', } );
            TweenLite.set('.socials', { 'overflow': 'hidden', } );
            TweenLite.to('.hidden-socials', animationSpeed, {
                'margin-right': 0,
            });
            TweenLite.to('.socials .share', animationSpeed, {
                rotation: 180,
            });
        }else {
            this.socialsOpened = false;
            $('.hidden-socials').removeClass('is-active');

            TweenLite.to('.hidden-socials', animationSpeed, {
                'margin-right': '-60px',
                'onComplete': function () {
                    TweenLite.to('.socials .share', animationSpeed, {
                        rotation: 0,
                    });
                }
            });

        }

    }

    aboutAuthor(close = null) {
        let animationSpeed = 0.5;

        if (close !== null) {
            this.aboutAuthorOpened = close;
        }

        if (!this.aboutAuthorOpened) {
            this.aboutAuthorOpened = true;
            TweenMax.set('.about-author-popup-container .content-container .content', {
               'margin-left': '100%',
            });
            TweenMax.set('.about-author-popup-container .inner-container', {
                'margin-right': '-100%',
            });
            $('.about-author-popup-container').addClass('is-active');

            TweenMax.to('.about-author-popup-container .inner-container', animationSpeed, {
                'margin-right': 0,
                'onComplete': function () {
                    TweenMax.to('.about-author-popup-container .content-container .content', animationSpeed/2, {
                        'margin-left': 0,
                    }, 0.1);
                }
            });

        }else {
            this.aboutAuthorOpened = false;
            TweenMax.to('.about-author-popup-container .inner-container', animationSpeed*4, {
                'margin-right': '-80%',
                'onComplete': function () {
                    $('.about-author-popup-container').removeClass('is-active');
                }
            });
        }
    }
}