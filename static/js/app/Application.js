class Application {
    constructor() {
        this.slider = null;

        this.initSlider();
        this.initAnimations();
        this.initFormValidation();
    }

    initSlider() {
        this.slider = $('.bxslider').bxSlider(
            {
                mode: 'fade',
                captions: false,
                controls:true,
                auto:true,
                autoHover:true,
                pager: false,
                speed:1000,
                pause:4000
            }
        );

    }

    initAnimations() {
        this.animations = new Animations();
    }

    /**
     * Animations
     */
    initCoverAnimations() {

    }

    /**
     * Browser events
     */
    onResize() {
        this.animations.onResize();
        this.animations.onScrollUp();
    }

    onScroll() {
        this.animations.onScroll();
    }

    onScrollUp() {
        this.animations.onScrollUp();
    }

    initFormValidation() {
        this.formValidation = new FormValidation();
    }
}

let application = null;
$(document).ready( () => {
   application = new Application();

   $(window).on('resize', () => {
       application.onResize();
   });

    lastScrollTop = 0;
    $(window).on('scroll', (e) => {
        application.onScroll();
        let st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
        if (st > lastScrollTop){
            // downscroll code
        } else {
            application.onScrollUp();
        }
        lastScrollTop = st;
    })
});
