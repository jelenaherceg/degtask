/**
 * Slider
 */
class Slider {
    constructor() {
        this.config = {
            element: '.application-slider'
        };
        this.element = null;

        this.init();
    }

    init() {
        this.element = $(this.config.element).bxSlider();
    }
}