const gulp = require('gulp');

//load gulp prefixed plugins
const $ = require('gulp-load-plugins')();

//load other plugins
const browserSync = require('browser-sync').create();
const gutil = require('gulp-util');
const babel = require('gulp-babel');
const es2015 = require('babel-preset-es2015');
const babelConfig = {
    presets: [es2015]
};
const sassConfig= {
    outputStyle: 'compressed',
    includePaths: [
        'node_modules/susy/sass',
    ]
};

const autoprefixConfig = {
    browsers: ['last 3 versions', 'ie >= 11'],
    cascade: false
};

const DIST_FOLDER = '../static/dist';
let paths = {
    pluginsJS: {
        src: [
            '../static/js/plugins/jquery/jquery.js',
            '../static/js/plugins/jquery.bxslider/js/jquery.bxslider.js',
            '../static/js/plugins/gsap/src/minified/TweenMax.min.js',
            '../static/js/plugins/gsap/src/minified/plugins/ScrollToPlugin.min.js',
        ],
        build: '../static/js/plugins/',
    },
    babelJs: {
        src: [
            '../static/js/app/base/*',
            '../static/js/app/*.js',
        ],
        build: '../static/js/babelified'
    },
    js: {
        src: [
            '../static/js/plugins/plugins.min.js',
            '../static/js/babelified/base/*',
            '../static/js/babelified/*.js',
        ],
        build: '../static/dist/',
    },
    scss: {
        src: [
            '../static/scss/main.scss',
            // '../static/scss/**/*'
        ],
        watch: [
            '../static/scss/main.scss',
            '../static/scss/**/*'
        ],
        build: '../static/dist/'
    },
};

//error handler
let onError = function (error) {
    //gutil.log(gutil.colors.red(error.message));
    //this.emit('end');
console.log(error);
    $.notify.onError({
        title: "Gulp Error in " + error.plugin,
        message: "\nError: " + error.message.substr(error.message.indexOf('static'))
    })(error);

    gutil.log();

    this.emit('end');
};

gulp.task('minPluginJs', function () {
    return gulp.src(paths.pluginsJS.src)
        .pipe($.plumber())
        .pipe($.concat('plugins.min.js', {newLine: ''}))
        .pipe($.uglify())
        .pipe(gulp.dest(paths.pluginsJS.build))
});

gulp.task('babelJs', function() {
   return gulp.src(paths.babelJs.src)
       .pipe($.plumber())
       .pipe(babel(babelConfig))
       // .pipe($.concat('es6.js', {newLine: ''}))
       .pipe(gulp.dest(paths.babelJs.build));
});
gulp.task('minJs', function () {
    return gulp.src(paths.js.src)
        .pipe($.plumber())
        .pipe($.concat('js.min.js', {newLine: ''}))
        .pipe($.uglify())
        .pipe(gulp.dest(paths.js.build))
});

gulp.task('minCss', function () {
    return gulp.src(paths.scss.src)
        .pipe($.plumber({errorHandler: onError}))
        .pipe($.sass(sassConfig))
        .pipe($.combineMq())
        .pipe($.autoprefixer(autoprefixConfig))
        .pipe($.cssnano())
        .pipe($.concat('style.min.css'))
        .pipe(gulp.dest(paths.scss.build));
        // .pipe(browserSync.stream());
});

/**
 * Watchers
 */
gulp.task('allWatcher', ['babelJs','minJs', 'minCss'], function () {
    gulp.watch(paths.babelJs.src, ['babelJs']);
    gulp.watch(paths.js.src, ['minJs']);
    gulp.watch(paths.scss.watch, ['minCss']);
});
gulp.task('jsWatcher', ['babelJs'], function () {
    gulp.watch(paths.babelJs.src, ['babelJs']);
});