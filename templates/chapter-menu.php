<div class="chapter-menu-container">
    <div class="chapter-menu-container-close"></div>
    <div class="chapter-menu">
        <h3 class="staggerAnimation"> Jump to chapters... </h3>
        <?= render('render/chapter-menu-link', [
            'chapterNumber' => '01',
            'chapterTitle' => 'Gregor Samsa woke from troubled dreams',
        ]) ?>

        <?= render('render/chapter-menu-link', [
            'chapterNumber' => '02',
            'chapterTitle' => 'At twilight Gregor wakes up',
        ]) ?>

        <?= render('render/chapter-menu-link', [
            'chapterNumber' => '03',
            'chapterTitle' => 'Gregors injury cripples him',
        ]) ?>

    </div>
</div>