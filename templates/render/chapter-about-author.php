<?php
?>

<div class="about-author-popup-container">
    <div class="inner-container">
        <div class="close"></div>
        <div class="about-author-popup">
            <div class="title">
                <h2>
                    About the author
                </h2>
                <span>
                 by Franz Kafka, first published in 1915.
                </span>
            </div>
            <div class="content-container">
                <div class="content">
                    <div class="first staggerAnimation">
                        <img src="static/img/kafka.png">
                    </div>
                    <div class="second staggerAnimation">
                        <p class="first-letter">
                            Kafka was born into a middle-class, German-speaking Jewish family in Prague, then part of
                            the
                            Austro-Hungarian Empire. In his

                            lifetime, most of the population of Prague spoke Czech, and the division between Czech- and
                            German-speaking people was a tangible reality, as both groups were strengthening their
                            national
                            identity.
                            The Jewish community often found itself in between the two sentiments, naturally raising
                            questions about a
                            place to which one belongs. Kafka himself was fluent in both languages, considering German
                            his
                            mother tongue.
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div><!--content-container-->
        </div><!--about-author-popup-->
    </div><!--inner-container-->

</div>
