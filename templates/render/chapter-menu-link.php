<?php
/** @var string $chapterNumber */
/** @var string $chapterTitle */
?>

<a href="#" class="chapter-menu-link staggerAnimation">
    <span class="chapter-menu-num"><?= $chapterNumber; ?></span>
    <span class="chapter-menu-title"><?= $chapterTitle ?></span>
    <span class="chapter-menu-white-line"></span>
</a>