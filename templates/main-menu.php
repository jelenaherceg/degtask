<div class="main-menu">
    <div class="transparent-background"></div>
    <div class="menu-container">
        <a class="menu-container-link staggerAnimation"> <span class="num"> 01 </span> <span class="txt"> About book </span></a>
        <a class="menu-container-link staggerAnimation"> <span class="num"> 02 </span> <span class="txt"> Authors </span></a>
        <a class="menu-container-link staggerAnimation"> <span class="num"> 03 </span> <span class="txt"> Extensions </span></a>
        <a class="menu-container-link staggerAnimation" href="contact.php"> <span class="num"> 04 </span> <span class="txt"> Contact us </span></a>

        <div class="connect-with-us staggerAnimation">
            <span>Connect with us</span>
           <div class="menu-container-socials">
               <a class="tw" target="_blank" href="http://www.twitter.com"></a>
               <a class="fb" target="_blank" href="http://www.facebook.com"></a>
               <a class="gp" target="_blank" href="http://plus.google.com"></a>
               <div class="clearfix"></div>
           </div>
        </div>
        <div class="menu-container-bottom-text staggerAnimation">
            <p>
                On the other hand,
                we denounce with righteous indignation
                and dislike men who are so beguiled and demoralized.
            </p>
        </div>
    </div>
</div><!--main-menu-->