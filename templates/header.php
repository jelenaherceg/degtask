<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta property="og:url"           content="http://degordianjunior.dev/" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="Metamorphosis" />
<meta property="og:description"   content="One morning, when Gregor Samsa woke from troubled dreams,
                        he found himself transformed in his bed into a horrible vermin." />
<meta property="og:image"         content="http://degordianjunior.dev/static/img/kafka.png" />

<script src="https://use.typekit.net/oyd2div.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
<link rel="stylesheet" type="text/css" href="static/dist/style.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

<title>
    Metamorphosis
</title>