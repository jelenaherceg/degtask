<div class="row">
    <div class="wrapper">
        <div class="chapter-container">
            <div class="chapter">
                <div class="chapter-number-container">
                    <strong class="chapter-number">02</strong>
                    <strong class="chapter-name">Chapter</strong>
                </div>
                <div class="chapter-content-wrapper">
                    <div class="content">
                        <h2>The lodgers notice</h2>
                        <p class="content-intro">
                            Gregor's room becomes a storage area for junk.
                            The family takes on three lodgers who bring their own furnishings,
                            so everything that isn't needed is tossed into Gregor's room.
                        </p>
                        <p class="content-text">
                            The bearded lodgers are extremely scrupulous about cleanliness and order and
                            attempt to arrange the apartment so that nothing unnecessary is lying around.
                            Because the lodgers usually eat at the apartment, the door to Gregor's room is
                            often kept closed. Even when it is opened, however, he often ignores it. He also stops eating almost entirely.
                            One day the charwoman leaves Gregor's door ajar while the lodgers are in the living room.
                            His mother and sister bring in food for them and they inspect it carefully before eating.
                            The family now eats in the kitchen. Then Gregor's father comes in and bows to the lodgers.
                            Gregor, watching them chew, realizes that he is starving to death because, lacking teeth,
                            he cannot eat human food. Grete begins playing the violin in the kitchen and the lodgers
                            ask her to come into the living room to play. His parents remain standing until the lodgers
                            offer the mother a chair. The lodgers, after listening for a short while, move over to the
                            window and begin whispering to show they are no longer interested and are disappointed with
                            the performance. Gregor, however, is drawn by it. Despite being covered with dirt from his
                            room, he crawls out, fantasizing about bringing his sister back into his room, making her
                            play for him, and then confiding that he had planned to send her to the Conservatory.
                        </p>
                    </div>
                    <div class="content-overlay">
                        <img src="static/img/overlay.png"/>
                    </div>
                </div><!--chapter-content-wrapper-->
            </div><!--chapter-->
        </div>
        <div class="chapter-nav">
            <a class="chapter-nav-prev">
                <div>
                    <strong>01</strong>
                </div>
                <span>Chapter</span>
            </a>
            <a class="chapter-nav-next">
                <div>
                    <strong>03</strong>
                </div>
                <span>Chapter</span>
            </a>
        </div>
    </div>
</div>