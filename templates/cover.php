<div class="row" id="cover">
    <div class="wrapper">
        <?php include 'main-menu.php';?>
        <?php include 'chapter-menu.php';?>

        <div class="cover">
            <div class="cover-container">
                <div class="header">
                    <a class="header-menu-link white-link" href="javascript:;">
                        <button id='id' class="c-hamburger c-hamburger-x">
                            <span></span>
                        </button>
                        <span> Menu </span>
                    </a>
                    <a class="header-chapters-link white-link" href="javascript:;">
                        <span> Chapters</span>
                    </a>
                </div>
                <div class="centered-text">
                    <h1 class="centered-text-title">Metamorphosis</h1>
                    <p class="centered-text-description">
                        <span>One morning, when Gregor Samsa</span> woke from troubled dreams,
                        he found himself transformed in his bed into a horrible vermin.
                    </p>
                    <i class="centered-text-divider"></i>
                </div><!--centered-text-->

            </div><!--cover-container-->
            <div class="footer">

                <div class="footer-relative-container">
                    <a class="white-link explore-more" href="javascript:;">
                        Explore more
                    </a>
                    <span class="copyright">
                            Degordian &#169; Copyright 2014
                            All rights are reserved
                        </span>

                    <div class="socials">
                       <?= render('render/socials', [])?>
                    </div>

                </div>
            </div><!--footer-->
        </div><!--cover-static-background-->
    </div><!--wrapper-->
</div><!--row-->
