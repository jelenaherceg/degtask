<?= render('main-menu', []);?>
<div class="row contact-wrapper">
    <div class="wrapper">
        <div class="contact">
            <div class="cover">
                <div class="cover-container">
                    <div class="header">
                        <a class="header-menu-link white-link" href="javascript:;">
                            <button id='id' class="c-hamburger c-hamburger-x">
                                <span></span>
                            </button>
                            <span> Menu </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="contact-inner">
                <h1 class="contact-title">Contact us</h1>
                <div class="contact-form">
                    <form class="form">
                        <div class="input-name">
                            <label for="name">
                                <span class="name">Name</span>
                                <span class="validation-message">Please enter your name</span>
                            </label>
                            <input type="text" name="" id="name" placeholder="Name">
                        </div>

                        <div class="input-email">
                            <label for="email">
                                <span class="name">Email</span>
                                <span class="validation-message">Please enter your email address</span>
                            </label>
                            <input type="text" name="" id="email" placeholder="Email">
                        </div>

                        <div class="input-message">
                            <label for="message">
                                <span class="name">Message</span>
                                <span class="validation-message">This value is required</span>
                            </label>
                            <textarea id="message" placeholder="Message" rows="10"></textarea>
                        </div>

                        <button class="send-button">Send</button>
                    </form>
                </div>
                <div class="contact-info">
                    <p>
                        <span>Telephone</span>
                        <span>+385 00 123 123</span>
                    </p>
                    <p>
                        <span>Email</span>
                        <span> info@metamorphosis.com</span>
                    </p>
                </div>
            </div>



            <div class="map">
                <h2 class="map-title">
                    Find us
                </h2>
                <div>
                    <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2780.5693610412!2d15.993406515944292!3d45.8198822791068!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4765d7a8cd202695%3A0xe06df175c20f0d6!2sDegordian!5e0!3m2!1sen!2shr!4v1491251476155"
                            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen>

                    </iframe>
                </div>
            </div><!--map-->

        </div><!--contact-->

    </div>
</div>